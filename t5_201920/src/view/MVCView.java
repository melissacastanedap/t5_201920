package view;

import model.logic.MVCModelo;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{

	}

	public void printMenu()
	{
		System.out.println("1. cargar datos");
		System.out.println("2. Buscar tiempos de viaje por trimestre, zona de origen y zona de destino (Tabla de Hash Linear Probing)");
		System.out.println("3. Buscar tiempos de viaje por trimestre, zona de origen y zona de destino (Tabla de Hash Separate Chaining)");
		System.out.println("4.  Pruebas.");

	}

	public void printMessage(String mensaje) {

		System.out.println(mensaje);
	}		

	public void printModelo(MVCModelo modelo)
	{
		System.out.println(modelo);		
	}






}
