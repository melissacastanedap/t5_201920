package controller;


import model.logic.MVCModelo;
import view.MVCView;


import java.util.Scanner;
import model.logic.*;
import view.*;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();

		modelo = new MVCModelo();
	}

	public void run() 
	{

		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		dato = lector.next();
		int hora = Integer.parseInt(dato);




		while( !fin ){
			view.printMenu();


			int option = lector.nextInt();
			switch(option){

			case 1:


				view.printMessage("Se empezaron a cargar los datos");

				dato = lector.next();

				view.printMessage( modelo.CargarTodosLosTrimestres() );


				view.printMessage("la zona  de origen del primer viaje es"+modelo.Primerviaje().darsourceId()+"La zona de destino es" +modelo.Primerviaje().dardestinID()+ "El tiempo promedio del viaje es" + modelo.Primerviaje().darmeanTravelTime()  );
				view.printMessage("la zona  de origen del ultimo viaje es"+modelo.Ultimoviaje().darsourceId()+"La zona de destino es" +modelo.Ultimoviaje().dardestinID()+ "El tiempo promedio del viaje es" + modelo.Ultimoviaje().darmeanTravelTime()  );




				break;

			case 2:


				view.printMessage("digite el trimestre");
				dato = lector.next();
				int trimestre = Integer.parseInt(dato);


				view.printMessage("digite la Zona Origen");
				dato = lector.next();
				int ZonaOrigen = Integer.parseInt(dato);


				view.printMessage("digite la Zona Destino");
				dato = lector.next();
				int ZonaDestino = Integer.parseInt(dato);






				for (int i = 0; i < modelo.R1_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_LinearProbing(trimestre, ZonaOrigen, ZonaDestino).length ; i++) 
				{

					UBERTrip buscar = modelo.mergeShow(trimestre, ZonaOrigen, ZonaDestino)[i];


					view.printMessage(" "+ trimestre + ZonaOrigen + ZonaDestino+ buscar.dardia()  + buscar.darmeanTravelTime() );
				}


				//trimestre, sourceid, dstid, dow, mean_travel_time


				break;

			case 3:


				view.printMessage("digite el trimestre");
				dato = lector.next();
				int trimestre1 = Integer.parseInt(dato);


				view.printMessage("digite la Zona Origen");
				dato = lector.next();
				int ZonaOrigen1 = Integer.parseInt(dato);


				view.printMessage("digite la Zona Destino");
				dato = lector.next();
				int ZonaDestino1 = Integer.parseInt(dato);



				for (int i = 0; i < modelo.R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining(trimestre1, ZonaOrigen1, ZonaDestino1).length ; i++) 
				{

					UBERTrip buscar = modelo.mergeShow(trimestre1, ZonaOrigen1, ZonaDestino1)[i];


					view.printMessage(" "+ trimestre1 + ZonaOrigen1 + ZonaDestino1+ buscar.dardia()  + buscar.darmeanTravelTime() );
				}



				break;

			case 4:



				long startTimeS = System.currentTimeMillis();				
				modelo.buscarTablaHashSC();
				long endTimeS = System.currentTimeMillis() - startTimeS;

				view.printMessage("El algoritmo se tomo:"+ endTimeS+"milisegundos para ordenar los viajes");




				break;






			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}
	}
}




