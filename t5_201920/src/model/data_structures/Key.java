package model.data_structures;

public class Key implements Comparable<Key>  
{

	public int key;

	public Key(int pKey) {
		key = pKey;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int pKey) {
		key = pKey;
	}

	@Override
	public int compareTo(Key o) {
		int nuevo=o.getKey();
		if(key>nuevo) {
			return 1;
		}
		if(key<nuevo) {
			return -1;
		}
		else {
			return 0;
		}
	}

}
