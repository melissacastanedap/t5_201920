package model.data_structures;

import java.util.Iterator;

public abstract class HashTable<K extends Comparable<K>,V> implements IHashTable<K, V> {
	 
	public K[] keys;
	public V[] values;
	public int M;		//tama�o de la tabla
	public int N;		//cantidad de duplas en la tabla
	
	public int rehashes;
	
	public HashTable(int pTamano){
		M = pTamano;
		N = 0;
		rehashes = 0;
		
		//Para hacer en las clases particulares
		//keys = new K[M];
		//values = new V[M];
	}
}
