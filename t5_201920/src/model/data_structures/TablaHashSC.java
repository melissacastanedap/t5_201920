package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class TablaHashSC <K, V> 
{
	//numero key- value pairs 
	private static  int n;

	//tamano del hash
	private static  int M;

	//crea la tabla
	private SequentialSearch<K, V>[] st;




	/*
	 * metodo constructor
	 */
	public TablaHashSC(int M) { // Create M linked lists.
		this.M = M;

		st = (SequentialSearch<K, V>[]) new SequentialSearch[n/M]; 

		for (int i = 0; i < M; i++)
			st[i] = new SequentialSearch();
	}


	/**
	 * resize the hash table to have the given number of chains,
    // rehashing all of the keys
	 * @param chains
	 */
	private void resize(int chains) 
	{
		TablaHashSC<K, V> temp = new TablaHashSC<K, V>(chains);

		for (int i = 0; i < M; i++) {
			for (K key : st[i].darKeys() ) {

				V valor= st[i].SeEncuentraElElemento(key);
				
				temp.putInSet(key, valor );
			}
		}
		this.M  = temp.size();
		this.n  = temp.darValor();
		this.st = temp.st;
	}

	
	public int size() 
	{
		return M;
	}
	
	public int darValor()
	{
		return n;
	}

	public boolean isEmpty() {
		return size() == 0;
	}


	
	public int hash(K key)
	{
		int hash;
		hash = key.hashCode() & 0x7fffffff % size();
		return hash;
	}


	
	public boolean contains(K key) 
	{
		boolean esta=false;
		
		if (key != null)
		{
			if(getSet(key)!=null)
			{
				esta = true;
			}
		}
			
			
		return esta;
	} 


	public void putInSet(K key, V value) 
	{

		int valorConHash = hash(key);

		if(st[valorConHash].SeEncuentraLaLlave(key))
		{
			M++;
			st[valorConHash].agregarKeyConValor(key, value);
		}


		if (n >= 10*M) resize(2*M);

		int i = hash(key);
		if (!st[i].SeEncuentraLaLlave(key)) n++;
		st[i].agregarKeyConValor(key, value); 
	}


	public V getSet(K key) 
	{
		V resp = null;

		if (key != null)
		{
			int a = hash(key);
			resp= st[a].SeEncuentraElElemento(key);
		}
		return resp;
	}


	public V deleteSet(K key) {

		if (key != null)
		{
		int i = hash(key);
		if (st[i].SeEncuentraLaLlave(key)) n--;
		st[i].eliminar(key);
		}
		// halve table size if average length of list <= 2
		if (M > (n/M) && n <= 2*M) resize(M/2);
		return null;
	}



	public Iterable<K> keys() {
		
		ArrayList<K> datos = new ArrayList<K>( );
		for (int i = 0; i < M; i++) {
			for (K key : st[i].darKeys())
				datos.add(key);
		}
		return datos;
	}


	public SequentialSearch<K,V>[] getHashTable()
	{
		return st;
	}
}
