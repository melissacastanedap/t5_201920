package model.data_structures;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class TablaHashLP extends HashTable<Key, Object>  {
	ListaEnlazada listaEnlazada;
	
	
	
	public TablaHashLP(int pTamano) {
		super(pTamano);
		/*1.Inicializiar keys y values.
		 * 2 llamar el metodo get de la clase ListaEnlazada para
		 * pedirle todos los viajes.
		 * 2.2 sacamos cada uno de los viajes y metemos los valores
		 * trimestre, sourceid y dstid 
		 */
		String key = null;
		
		ArrayList listaViajes= listaEnlazada.darDatos();
		for(int i =0;i< listaViajes.size(); i++) {
			Object actual=listaViajes.get(i);
			
		}
	}
	
	public Key CrearLlave(int pTrimestre,int pSourceId, int pDsidt ) {
		String llave=pTrimestre+"\n"+"-"+"\n"+pSourceId+"\n"+pDsidt;
		int key= Integer.parseInt(llave);
		
		return key;
	}

	@Override
	public void putInSet(Key key, Object value) {
		/*
		 * 1. Verificar el factor de carga
		 * 	1.1 Si el factor de carga es mayor al permitido => rehash
		 * 2. Encontrar la primera posicion vacia en la tabla desde key.hash
		 * 3. Insertar o actualizar dependiendo del caso
		 */
		if(!(N/M<=0.75)) {
			rehash();
		}
		
		int pos = hash(key);
		//Si est� vac�o, insertar directamente
		if(keys[pos]==null) {
			keys[pos]= key;
			values[pos]= value;
			N++;
			return;
		}
		//Si no est� vac�o, linear probing
		else {
			for(pos = hash(key); keys[pos] != null ; pos = (pos + 1) % M) {
				if(key.equals(keys[pos])) {
					values[pos]=value;
					return;
				}
			}
			keys[pos]= key;
			values[pos]= value;
			N++;
			return;			
		}

	}

	public void rehash() {
		/*1.1Hacer copia de seguridad de datos
		 * 2.1. Encontrar el siguiente M primo
		 * 2.2. Reinicializar tabla con nuevo tama�o
		 * 3 Meter la copia de seguridad en la nueva tabla
		 * 3.1 Recorre la copia de seguridad
		 * 3.2 Insertar en 2.2
		 */
		rehashes++;
		//1.1
		Object[] copiaValues= values;
		Key[] copiaKey=keys;
		int tamano= M;
		//2.1
		int nuevoTamano=nuevoTamano(M);
		//2.2 Se sobreescribio 
		values= new Object [nuevoTamano];
		keys = new Key[nuevoTamano];
		N=0;
		//3.1
		for(int i=0;i<values.length; i++) {
			//Insertar cada dupla (keys[i], values[i]))
			if(copiaKey[i]!=null) {
				putInSet(copiaKey[i], copiaValues[i]);
			}
		}

	}

	public int nuevoTamano(int nuevoNumero) {
		/*1.Variable nuevoPrimo, es una variable de respuesta
		 * 2.Recorremos con un while que tiene condicion si esPrimo 
		 * 3.
		 */
		int nuevoPrimo=nuevoNumero +1;
		while(/*condicion para seguir buscando*/ !esPrimo(nuevoPrimo)){
			/*Qu� hacer en cada iteracion*/
			nuevoPrimo++;		
		}	
		return nuevoPrimo;
	}

	public boolean esPrimo(int nuevoNumero) {
		/* Hacer un for 
		 * condicional if que verifica divisibilidad
		 * retornamos si es primo o no
		 */
		for(int i=2;i<nuevoNumero;i++) {
			if(nuevoNumero%i==0) {
				return false;
			}
		}
		return true;
	}
	
	public int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % M;
	}
	@Override
	//public Iterator<Object> getSet(Key key) {
	public Object getSet(Key key) {
		/*1. Hacer un for
		 * 
		 */
		//1.1
		for(int i = hash(key); keys[i] != null; i = (i + 1)%M) {
			if(key.equals(keys[i])) {
				return values[i];
			}
		}
		return null;
	}

	@Override
	public Object deleteSet(Key key) {
		// 1. Borrar el valor que corresponde a key
		int posicionVacia = -1;
		Object respuesta= null;
		for(int i = hash(key); keys[i] != null; i = (i + 1)%M) {
			if(key.equals(keys[i])) {
				respuesta= values[i];
				keys[i]= null;
				values[i]= null;
				posicionVacia = i;
				N--;
				
			}
		}
		
		// 2. "Subir" los valores 
		/*2.1 Hacemos copia de seguridad
		 * 2.2 Borro lo que copie, borro el orginal
		 * tanto para keys como para values
		 * 2.3 
		 */
		int posicionInicial= posicionVacia+1;
		//2.1
		while(keys[posicionInicial] != null) {
			Key copiaSeguridadKey = keys[posicionInicial];
			Object copiaSeguridadValue = values[posicionInicial];
			keys[posicionInicial]=null;
			values[posicionInicial]= null;
			//Toca disminuir la cantidad de cosas en la tabla
			N--;
			putInSet(copiaSeguridadKey, copiaSeguridadValue);
			posicionInicial++;
		}
		return respuesta;
	}

	@Override
	public Iterator<Key> Keys() {
		
		return Arrays.asList(keys).iterator();
	}

}