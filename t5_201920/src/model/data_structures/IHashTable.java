package model.data_structures;

import java.util.Iterator;

public interface IHashTable<K extends Comparable<K>,V> {

	 public void putInSet(K key, V value);
	 
	 //public Iterator<V> getSet(K key);
	 public V getSet(K key);
	 
	 //public Iterator<V> deleteSet(K key);
	 public V deleteSet(K key);
	 
	 public Iterator<K> Keys();
}
