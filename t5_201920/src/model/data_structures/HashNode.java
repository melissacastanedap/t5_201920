package model.data_structures;

public class HashNode< K,V > 
{
	
	
	K  Key;
	
	V Value;
	
	private HashNode< K,V > siguiente;
	
	
	/**
	 * 
	 * @param elemento
	 * @param siguiente
	 */
	public HashNode (K key, V Value)
	{
		this.Key = key;
		
		this.Value = Value;
	}
	
	
	
	public K dar_Key()
	{
		return Key;
	}
	
	public void cambiarValor(V nuevo)
	{
		Value = nuevo;
	}

	public V darValor() 
	{
		return Value;
	}
	
	
	public void CambiarSiguiente(HashNode<K, V> node)
	{
		siguiente = node;
	}
	public HashNode<K, V> darSiguiente()
	{
		return siguiente;
	}

	
}
