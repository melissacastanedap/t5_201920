package model.data_structures;

import java.util.ArrayList;

import org.junit.rules.Verifier;

public class SequentialSearch <K,V>
{
	private HashNode<K, V> primero;
	private int elemento;
	private int size;


	public SequentialSearch (  )
	{
		elemento = 0;
		primero=null;
	}


	public int darElemento()
	{
		return elemento;
	}

	public int darSize()
	{
		return size;
	}

	public HashNode<K, V> darprimero()
	{
		return primero;
	}


	public Iterable<K> darKeys( )
	{
		ArrayList<K> datos = new ArrayList<K>( );
		HashNode<K,V> actual = primero;

		while( actual != null )
		{
			datos.add( (K) actual );
			actual = actual.darSiguiente( );
		}
		return datos;
	}



	public boolean IsEmpty()
	{
		boolean resp;

		if(primero==null){
			resp = true;
		}
		else{
			resp = false;
		}
		return resp;
	}



	public V SeEncuentraElElemento (K key)
	{
		V resp=null;
		if(primero!=null)
		{
			HashNode<K, V> verificador = primero;

			while(verificador!=null)
			{
				if(verificador.dar_Key().equals(key) )
				{
					resp = verificador.darValor();
				}
				verificador = verificador.darSiguiente();
			}
		}
		return resp;
	}




	public void agregarKeyConValor (K Key, V Value) 
	{
		boolean yaLoAgrego = false;
		HashNode<K, V> aAgregar = new HashNode(Key, Value);

		if(aAgregar!=null)
		{

			if(primero == null)
			{
				primero = aAgregar;
				size++;
			}
			else
			{
				HashNode<K, V> verificar = primero;

				while(verificar!=null && !yaLoAgrego && verificar.darSiguiente()!=null)
				{
					if(verificar.dar_Key().equals(Key) | verificar.dar_Key()== Key)
					{
						verificar.cambiarValor(Value);
						yaLoAgrego = true;
					}
					else if(!yaLoAgrego)
					{
						verificar.CambiarSiguiente(aAgregar);
						size++;
					}
				}
			}
		}

	}


	public boolean SeEncuentraLaLlave( K key)
	{
		if (SeEncuentraElElemento(key) != null)
		{
			return true;
		}
		else{
			return false;
		}
	}


	public  V eliminar (K Key)
	{
		V elementoEliminado=null;

		if(primero!=null)
		{

			HashNode<K, V> verificar = primero;

			if(verificar.dar_Key().equals(Key) | verificar.dar_Key()== Key)
			{
				if(verificar.darSiguiente()==null)
				{
					primero=null;
				}
				else
				{
					primero= verificar.darSiguiente();
				}
				elementoEliminado = verificar.darValor();
				size--;	
			}
			else
			{
				while(verificar.darSiguiente()!=null )
				{
					if(verificar.darSiguiente().dar_Key().equals(Key) | verificar.darSiguiente().dar_Key()==Key)
					{
						verificar.CambiarSiguiente(verificar.darSiguiente().darSiguiente());
						size--;
						elementoEliminado = verificar.darValor();
					}
					else if(verificar.darSiguiente().darSiguiente()==null && verificar.darSiguiente().dar_Key().equals(Key)) 
					{
						verificar.CambiarSiguiente(null);
						size--;
						elementoEliminado = verificar.darValor();
					}

					verificar = verificar.darSiguiente();
				}	
			}	
		}
		return elementoEliminado;
	}

	
	
	

}
