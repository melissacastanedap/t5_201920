package model;

public interface IListaEnlazada <T extends Comparable<T>>
{

	int size ();

	void agregar(T dato);

	T eliminar(T dato);

	void remplazar(T dato, T dato2);

	T buscar (int pos);
	
	T darPrimero();
	
	T darUltimo();
	

	

}
