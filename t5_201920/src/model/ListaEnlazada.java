package model;

import java.util.ArrayList;
import java.util.Iterator;

import model.logic.UBERTrip;

//import model.logic.UBERTrip;


public class ListaEnlazada<T extends Comparable<T>> implements IListaEnlazada<T>

{


	//------------
	//ATRIBUTOS
	//-----------
	private Nodo primero;

	private T elementos[];

	private int size;




	//------------
	//CONSTRUCTOR 
	//-----------
	public ListaEnlazada(  )
	{
		primero = null;
		size = 0;
	}



	//------------
	//METODOS
	//-----------

	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#size()
	 */
	public int size()
	{
		return size;
	}


	/*
	 * 
	 */
	public ArrayList<T> darDatos( )
	{
		ArrayList<T> datos = new ArrayList<T>( );
		Nodo<T> actual = primero;
		while( actual != null )
		{
			datos.add( (T) actual );
			actual = actual.darSiguiente( );
		}
		return datos;
	}


	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#agregar(java.lang.Comparable)
	 */
	public void agregar(T elemento)
	{

		Nodo<T> nuevo = new Nodo<T>(elemento, null);

		if (primero==null)
		{
			//Lista vacia
			primero = nuevo;
			size++;

		}
		else if(primero.compareTo(elemento)> 0 )
		{
			nuevo.cambiarSiguiente(primero);
			primero= nuevo;
			size++;

		}
		else
		{
			Nodo<T> NodoSi = primero; 

			while(NodoSi.darSiguiente()!=null )
			{
				if(NodoSi.darSiguiente().compareTo(elemento) >0 )
				{
					nuevo.cambiarSiguiente(NodoSi.darSiguiente());
					NodoSi.cambiarSiguiente(nuevo);
					size++;
				}

				NodoSi= NodoSi.darSiguiente();
			}
			NodoSi.cambiarSiguiente(nuevo);
			size++;
		}
	}




	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#eliminar(java.lang.Comparable)
	 */
	public T eliminar(T dato)
	{
		T elementoEliminado=null;

		if(primero!=null)
		{

			Nodo<T> nodoSi = primero; 

			if(nodoSi.obtenerDato().equals(dato))
			{
				if(nodoSi.darSiguiente()==null)
				{
					primero=null;
				}
				else
				{
					primero= nodoSi.darSiguiente();
				}
				elementoEliminado = nodoSi.obtenerDato();
				size--;
			}

			else
			{
				while(nodoSi.darSiguiente()!=null )
				{
					if(nodoSi.darSiguiente().obtenerDato().equals(dato))
					{
						nodoSi.cambiarSiguiente(nodoSi.darSiguiente().darSiguiente());
						size--;
						elementoEliminado = nodoSi.obtenerDato();
					}
					else if(nodoSi.darSiguiente().darSiguiente()==null && nodoSi.darSiguiente().obtenerDato().equals(dato)) 
					{
						nodoSi.cambiarSiguiente(null);
						size--;
						elementoEliminado = nodoSi.obtenerDato();
					}

					nodoSi = nodoSi.darSiguiente();
				}
			}
		}
		return elementoEliminado;

	}



	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#remplazar(java.lang.Comparable, java.lang.Comparable)
	 */
	@SuppressWarnings("unchecked")
	public void remplazar(T datoact, T nuevoele  )
	{
		if(primero!=null)
		{
			Nodo<T> nodoSi = primero; 

			while(!nodoSi.obtenerDato().equals(datoact) )
			{
				nodoSi= nodoSi.darSiguiente();
			}
			if(nodoSi != null)
				nodoSi.cambiarDato(nuevoele);
		}
	}



	/*
	 * (non-Javadoc)
	 * @see model.data_structures.IListaEnlazada#buscar(int)
	 */
	@SuppressWarnings("unchecked")
	public T buscar(int pos )
	{
		T eleBuscado = null;

		if(primero!=null && pos>=0 && pos<size)
		{
			if(pos==0)
			{
				eleBuscado = (T) primero.obtenerDato();
			}
			else
			{
				int contador = 0;
				Nodo<T> nodoSi = primero; 

				while(nodoSi.darSiguiente()!=null && contador!=pos)
				{
					nodoSi=nodoSi.darSiguiente();
					contador++;
				}
				eleBuscado= (T) nodoSi.obtenerDato();
			}

		}

		return eleBuscado ;
	}



	public T darPrimero()
	{
		return ((T) primero);
	}

	
	

	public T darUltimo()
	{
		T aux = null;

		Nodo<T> nodoSi = primero; 

		while(nodoSi.darSiguiente()!=null)
		{
			nodoSi=nodoSi.darSiguiente();
			
		}
		aux = (T) nodoSi;
		
		
		return (T) primero;
	}


	
	
	

	public T [] toArrayFacts() 
	{
		
		 T[] arreglo = (T[]) new Object[size];
		 
		return arreglo;
	}








}