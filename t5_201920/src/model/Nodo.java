package model;

public class Nodo <T extends Comparable<T>>
{
	
	
	
	private T elemento ;
	
	private Nodo<T> siguiente;
	
	
	
	
	/**
	 * 
	 * @param elemento
	 * @param siguiente
	 */
	public Nodo(T elemento, Nodo<T> siguiente)
	{
		this.elemento = elemento;
		
		this.siguiente = siguiente;
	}

	
	/**
	 * 
	 * @return
	 */
	public Nodo<T> darSiguiente()
	{
		return siguiente;
	}
	
	
	/**
	 * 
	 * @param sigNode
	 */
	public void cambiarSiguiente(Nodo<T> sigNode)
	{
		siguiente = sigNode;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public T obtenerDato()
	{
		return elemento;
	}
	
	/**
	 * 
	 * @param dato
	 */
	public void cambiarDato(T dato)
	{
		elemento = dato;
	}
	
	
	/**
	 * 
	 * @param dato
	 * @return
	 */
	public int compareTo(T dato) 
	{

		if(elemento.compareTo(dato)>0 ){
			return 1;
		}
		else if (elemento.compareTo(dato)==0){
			return 0;
		}
		else{
			return -1;
		}
	}
}
