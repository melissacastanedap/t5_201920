package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import model.IListaEnlazada;
import model.ListaEnlazada;
import model.data_structures.*;
import model.logic.*;;


/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{




	public final static String DATOS_PRIMER_TRIMESTRE  ="./data/bogota-cadastral-2018-1-WeeklyAggregate.csv";
	public final static String DATOS_SEGUNDO_TRIMESTRE ="./data/bogota-cadastral-2018-2-WeeklyAggregate.csv";
	public final static String DATOS_TERER_TRIMESTRE   ="./data/bogota-cadastral-2018-3-WeeklyAggregate.csv";
	public final static String DATOS_CUARTO_TRIMESTRE  ="./data/bogota-cadastral-2018-4-WeeklyAggregate.csv";




	/**
	 * Atributos del modelo del mundo
	 * que consiste en la lista de datos que son de tipo uberTRIP
	 */


	private IListaEnlazada<UBERTrip> datosLista;

	private SequentialSearch ListaSecuencial;

	private int tamano;

	private TablaHashSC tablaHashS= new TablaHashSC( tamano );

	private TablaHashLP tablaHashL = new TablaHashLP(tamano);

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{


		ListaSecuencial = new SequentialSearch();

		tablaHashS = new TablaHashSC<>(tamano);

		tablaHashL = new TablaHashLP(tamano);
		datosLista = new ListaEnlazada<UBERTrip>() ;


	}



	public int listatamano()
	{
		return tablaHashL.nuevoTamano(tamano);
	}


	public int dartamanoListaSecuencial()
	{
		return tablaHashS.size();
	}



	/**
	 * Se cargan todos los trimestres de 4 diferentes archivos 
	 * @return se retorna los 4 trimestres cargados concatenados en un int
	 */

	public String CargarTodosLosTrimestres()
	{
		String resp;


		int a =loadTravelTimes(DATOS_PRIMER_TRIMESTRE);
		int b =loadTravelTimes(DATOS_SEGUNDO_TRIMESTRE);
		int c =loadTravelTimes(DATOS_TERER_TRIMESTRE);
		int d =loadTravelTimes(DATOS_CUARTO_TRIMESTRE);

		resp = "Numero de viajes primer archivo"+a+"Numero de viajes primer archivo"+b+"Numero de viajes primer archivo"+c+"Numero de viajes primer archivo"+d;


		return resp;

	}


	/**
	 * Este metodo se encarga de cargar el archivo pasado por parametro
	 * @param arcivhoa
	 * @return
	 */
	public int loadTravelTimes(String arcivhoa)
	{

		File archivo = new File(arcivhoa);

		FileReader lector;

		int totalViajes=0;

		int trimestre =0;

		if( arcivhoa.equals(DATOS_PRIMER_TRIMESTRE) )
		{
			trimestre=1;
		}
		if( arcivhoa.equals(DATOS_PRIMER_TRIMESTRE) )
		{
			trimestre=2;
		}
		if( arcivhoa.equals(DATOS_PRIMER_TRIMESTRE) )
		{
			trimestre=3;
		}
		if( arcivhoa.equals(DATOS_PRIMER_TRIMESTRE) )
		{
			trimestre=4;
		}


		try {

			lector = new FileReader(archivo);


			BufferedReader bf = new BufferedReader(lector);

			String linea = bf.readLine();
			bf.readLine();

			while(linea != null )
			{
				//*Se encarga de separa por una ; cada una de las lineas
				String[] datos = linea.split(",");


				int sourceid = Integer.parseInt(datos[0]);
				int dstid = Integer.parseInt(datos[1]);
				int DHM = Integer.parseInt(datos[2]);
				double meanTravelTime = Double.parseDouble(datos[3]);
				double standardDeviationTravelTime =  Double.parseDouble(datos[4]);
				double geometricMeanTravelTime=  Double.parseDouble( datos[5]);
				double geometricStandardDeviationTravelTime=  Double.parseDouble(datos[6]);


				UBERTrip viaje = new UBERTrip(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

				//Ayuda: Puede usar como llave un String que sea la concatenación de trimestre, sourceid y dstid separados por “-”.

				String llave =( trimestre+"-"+ viaje.darsourceId()+"-"+viaje.dardestinID());




				datosLista.agregar(viaje);

				ListaSecuencial.agregarKeyConValor(llave, viaje);

				tablaHashS.putInSet(llave, viaje);

				tablaHashL.putInSet(llave, viaje);

				linea = bf.readLine();
				totalViajes++;
				tamano++;


			}
		}
		catch (Exception e) {

			e.printStackTrace();
		}
		return totalViajes;
	}




	public UBERTrip Primerviaje()
	{
		return datosLista.darPrimero() ;
	}


	public UBERTrip Ultimoviaje()
	{
		return datosLista.darUltimo();
	}




	public UBERTrip[] R1_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_LinearProbing(int trimestre, int ZonaOrigen, int ZonaDestino   )
	{
		String Key = ( trimestre+"-"+ ZonaOrigen+"-"+ZonaDestino);

		UBERTrip[] resp = new UBERTrip[tamano];


		if (tablaHashL.getSet(Key)==true)
		{
			resp = (UBERTrip[]) tablaHashL.getSet(Key);
		}

		return resp;
	}



	/**
	 * Dado un trimestre, zona de origen y zona destino retornar los tiempos de sus viajes y los dias respectivos. 
	 * El resultado debe estar ordenado por día de la semana (dow). 
	 * De cada viaje en el resultado se debe mostrar:trimestre, sourceid, dstid, dow, mean_travel_time
	 */
	public UBERTrip[] R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining (int trimestre, int ZonaOrigen, int ZonaDestino   )
	{
		String Key = ( trimestre+"-"+ ZonaOrigen+"-"+ZonaDestino);

		UBERTrip[] resp = new UBERTrip[tablaHashS.size()];

		if ( tablaHashS.contains(Key) == true  )
		{
			resp = (UBERTrip[]) tablaHashS.getSet(Key);
		}

		return resp;

	}











	/**
	 * METODO DE ORDENAMIENTO MERGE SORT 
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo Merge sort.
	 * el algoritmo se obtuvo de ayuda de videos de youtube y de ejemplos del libro
	 * @param hora
	 * @return
	 */
	public UBERTrip[] mergeShow(int trimestre, int ZonaOrigen, int ZonaDestino )
	{
		UBERTrip[] listaConDatos = new UBERTrip[  R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining(trimestre, ZonaOrigen, ZonaDestino).length];



		return mergeSort(0, listaConDatos.length-1, trimestre, ZonaDestino, ZonaDestino, ZonaDestino);
	}


	/**
	 * 
	 *Ordenar ascendentemente los viajes resultantes de la consulta del punto 3 (en el arreglo de objetos comparables) usando el algoritmo MergeSort.
	 * @param plow
	 * @param phigh
	 * @param hora
	 * @return
	 */
	public UBERTrip[] mergeSort( double plow, double phigh, int dia, int trimestre, int ZonaOrigen, int ZonaDestino)
	{

		UBERTrip[] aux = new UBERTrip[R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining(trimestre, ZonaOrigen, ZonaDestino).length];

		if(plow >= phigh)
		{
			return aux;
		}

		double  middle = (plow+ phigh)/2;

		mergeSort( plow, middle,dia, ZonaDestino, ZonaDestino, ZonaDestino);

		mergeSort( middle+1, phigh, dia, ZonaDestino, ZonaDestino, ZonaDestino);

		merge (  plow,middle, phigh, dia, ZonaDestino, ZonaDestino, ZonaDestino);

		return aux;


	}

	/**
	 * 
	 * @param Plow
	 * @param Pmiddle
	 * @param Phigh
	 * @param hora
	 */
	private void merge ( double Plow, double Pmiddle, double Phigh, int hora, int trimestre, int ZonaOrigen, int ZonaDestino )
	{
		UBERTrip[] listaAOrdenar = new UBERTrip[R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining(trimestre, ZonaOrigen, ZonaDestino).length];

		UBERTrip[] aux = new UBERTrip[R2_BuscartiemposDeViajePorTrimestre_ZonaOrigen_ZonaDestino_Tabla_SeparateChaining(trimestre, ZonaOrigen, ZonaDestino).length];

		double i = Plow;
		double j = Pmiddle+1;
		double k = Plow;

		while ((i <= Pmiddle) &&  (j <= Phigh))
		{   
			if (listaAOrdenar[(int) i].compareTo(listaAOrdenar[(int) j])==(-1|0)){

				listaAOrdenar[(int) k]= aux[(int) i];
				i++;
			}

			else{

				listaAOrdenar[(int) k]= aux[(int) j];
				j++;
			}
		}

		k++;

		while(i<= Pmiddle) {
			listaAOrdenar[(int) k]= aux[(int) i];
			k++;
			i++;
		}

		while(j<= Phigh) {
			listaAOrdenar[(int) k]= aux[(int) j];
			k++;
			j++;
		}
	}




	public UBERTrip[] generarMustera () 
	{
		int i=0 , cantidadDatos = 10000;

		TablaHashSC tablaHashS= new TablaHashSC(cantidadDatos);

		TablaHashLP tablaHashL = new TablaHashLP(cantidadDatos);

		UBERTrip[] resp = new UBERTrip[tablaHashS.size()];
		UBERTrip[] Muestra = new UBERTrip[cantidadDatos];


		for ( i = 1; i < 8000; i++) 
		{	

			int sourceid = (int) (Math.random()*10000);
			int dstid = (int) (Math.random()*10000);
			int DHM = (int) (Math.random()*10000);
			double standardDeviationTravelTime = (Math.random()*10000);
			double geometricMeanTravelTime=  (Math.random()*10000);
			double geometricStandardDeviationTravelTime=  (Math.random()*10000);

			double meanTravelTime =  (Math.random()*10000);


			UBERTrip viaje = new UBERTrip(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

			resp[i] = viaje;


		}

		for (int j2 = 0; j2< 2000; j2++) 
		{

			int sourceid = (int) (Math.random()*100000);
			int dstid = (int) (Math.random()*100000);
			int DHM = (int) (Math.random()*100000);
			double standardDeviationTravelTime = (Math.random()*100000);
			double geometricMeanTravelTime=  (Math.random()*100000);
			double geometricStandardDeviationTravelTime=  (Math.random()*100000);

			double meanTravelTime =  (Math.random()*100000);


			UBERTrip viaje = new UBERTrip(sourceid, dstid, DHM, meanTravelTime, standardDeviationTravelTime, geometricMeanTravelTime, geometricStandardDeviationTravelTime);

			if(  sourceid<10000 && dstid<10000 && DHM<10000 && standardDeviationTravelTime<10000 && geometricMeanTravelTime<10000 && geometricStandardDeviationTravelTime<10000  )

				resp[i] = viaje;

		}
		return resp;
	}



	public void buscarTablaHashSC()
	{
		TablaHashSC tablaHashS= new TablaHashSC(generarMustera().length );

		int Key = (int) (Math.random()*10000);

		if ( Key>0 && Key < 10000)
		{
			tablaHashS.getSet(Key);
		}

	}


}



