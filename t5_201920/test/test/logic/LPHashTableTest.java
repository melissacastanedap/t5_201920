package test.logic;



import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.*;

/**
 * Pruebas para la tabla de hash que utiliza linear probing para solucionar colisiones
 * @author pa.suarezm
 */
public class LPHashTableTest extends TestCase{

	private static final Key Llave = null;

	//Atributos

	
	/**
	 * La tabla de prueba
	 * Las llaves para esta tabla son Key y almacena valores de tipo Object
	 */
	private IHashTable<Key,Object> tabla;
	
	
	//Escenarios
	
	
	/**
	 * Inicializa la tabla vacia con un tama�o de 5
	 */
	public void setUp(){
		tabla = new TablaHashLP(5);
	}
	

	//Tests

	
	/**
	 * Caso 1: Agrega una dupla que no existe
	 * Caso 2: Agrega una dupla en donde la llave ya existe 
	 */
	public void testPut(){
		setUp();
		
		//Caso 1
		Key  llave1 = Llave ;
		tabla.putInSet(llave1, 1);
		assertEquals("El valor no se agreg� bien", 
						1, tabla.getSet(llave1));
		
		//Caso 2
		tabla.putInSet(llave1, 2);
		assertEquals("El valor no se agreg� bien",
						Integer.valueOf(2), tabla.getSet(llave1));

	}
	
	/**
	 * Caso 1: Return el valor agregado anteriormente con la misma llave
	 * Caso 2: Return null con una llave que no existe
	 */
	public void testGet(){
		setUp();
		
		//Caso 1
		Key llave1 = Llave;
		tabla.putInSet(llave1, 1);
		assertEquals("No retorn� el valor esperado", Integer.valueOf(1), tabla.getSet(llave1));
		
		//Caso 2
		assertNull("No retorn� el valor esprado", tabla.getSet(llave1));
	}
	
	/**
	 * Caso 1: Borra un valor que existe en la tabla
	 * Caso 2: Intenta borrar un valor que no existe en la tabla
	 */
	public void testDelete(){
		setUp();
		
		//Caso 1
		Key llave1 = Llave;
		tabla.putInSet(llave1, 1);
		assertEquals("No devuelve el valor eliminado", Integer.valueOf(1), tabla.deleteSet(llave1));
		assertNull("No elimin� el valor", tabla.getSet(llave1));
		
		//Caso 2
		assertNull("No devuelve el valor esperado", tabla.getSet(llave1));
		
	}
	
	/**
	 * Caso 1: Verifica que lo que retorne keys() sea una instancia de Iterator y que no sea null
	 */
	public void testKeys(){
		setUp();
		
		assertTrue("Deberia retornar un Iterator", (tabla.Keys() instanceof Iterator<?>) 
				&& (tabla.Keys()!=null));
	}
	
	
}
